<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://rsms.me/inter/inter.css">
		<link rel="stylesheet" href="../style.css">
		<?php
			$result = 0;
			$first_number = (int)$_POST['firstNumber'];
			$second_number = (int)$_POST['secondNumber'];

			if($first_number > 0 &&  $second_number > 0){
				if(isset($_POST['add'])){
					$result = $first_number + $second_number;
				} else if(isset($_POST['minus'])){
					$result = $first_number - $second_number;
				} else if(isset($_POST['multiply'])){
					$result = $first_number * $second_number;
				} else if(isset($_POST['divide'])){
					$result = $first_number / $second_number;
				}
			}
		?>
	</head>
	<body>
		<div class="min-h-screen bg-white">
			<main>
				<!-- Side-by-side grid -->
				<div class="bg-white">
					<div class="max-w-md mx-auto py-12 px-4 sm:max-w-3xl sm:py-10 sm:px-6 lg:max-w-7xl lg:px-8">
						<a href="../" class="self-end text-base text-indigo-600 font-semibold  uppercase">Home</a>
						<br>
						<div class="mt-5 md:mt-0 md:col-span-2">
							<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
								<div class="shadow sm:rounded-md sm:overflow-hidden">
									<div class="px-4 py-5 bg-white space-y-6 sm:p-6">
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Enter First Number </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="text" name="firstNumber" id="company-website" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300" placeholder="e.g 55">
												</div>
											</div>
											<div class="col-span-3 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Enter Second Number </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="text" name="secondNumber" id="company-website" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300" placeholder="e.g 50">
												</div>
											</div>
										</div>
										<div>
											<div class="grid grid-cols-6 gap-6">
												<div class="col-span-2 sm:col-span-2">
													<label for="company-website" class="block text-sm font-medium text-gray-700"> Result </label>
													<div class="mt-1 flex rounded-md shadow-sm">
														<input type="text" value="<?php echo $result; ?>" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300" disabled>
													</div>
												</div>
											</div>
										</div>
										<div>
											<div class="mt-1 flex items-left">
												<input type="submit" name="add" value="Add" class=" bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
												<input type="submit" name="minus" value="Minus" class="ml-5 bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
												<input type="submit" name="multiply" value="Multiply" class="ml-5 bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
												<input type="submit" name="divide" value="Divide" class="ml-5 bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
											</div>
										</div>
									</div>
									<div class="px-4 py-3 bg-gray-50 text-left sm:px-6">
										<h3>The four basic operations of arithmetic (PHP)</h3>
										<span class=" text-gray-500 text-sm">
										Display 2 text boxes in a page. If you enter numbers in the text box and press one of the submit buttons, it will calculate and show the result according to the operation.
										</span>
									</div>
								</div>
							</form>
    					</div>
					</div>
				</div>
			</main>
		</div>
	</body>
</html>
