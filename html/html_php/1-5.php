<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://rsms.me/inter/inter.css">
		<link rel="stylesheet" href="../style.css">
		<?php
			$input_date = "";
			if(isset($_POST['submit']))
			{
				$input_date = $_POST['date'];
			}

			function getPreviousDates($input_date){
				for($i = 1; $i <= 3; $i++){
					$date = date('Y-m-d', strtotime($input_date. " - $i days"));
					echo "<p>".$date."-".date('l', strtotime($date))."</p>";
				}
			}
		?>
	</head>
	<body>
		<div class="min-h-screen bg-white">
			<main>
				<!-- Side-by-side grid -->
				<div class="bg-white">
					<div class="max-w-md mx-auto py-12 px-4 sm:max-w-3xl sm:py-10 sm:px-6 lg:max-w-7xl lg:px-8">
						<a href="../" class="self-end text-base text-indigo-600 font-semibold  uppercase">Home</a>
						<br>
						<div class="mt-5 md:mt-0 md:col-span-2">
							<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
								<div class="shadow sm:rounded-md sm:overflow-hidden">
									<div class="px-4 py-3 bg-gray-50 text-left sm:px-6">
										<h3>Input date. Then show 3 days from the input date and the days of the week of each date. (PHP)</h3>
										<span class=" text-gray-500 text-sm">
											Display 1 text box in a page. If you enter the date in the text box and press the submit button, it will calculate and show the result.
										</span>
									</div>
									<div class="px-4 py-5 bg-white space-y-6 sm:p-6">
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Enter First Number </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="date" name="date" id="company-website" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300" placeholder="e.g 55">
												</div>
											</div>
										</div>
										<div>
											<div class="mt-1 flex items-left">
												<input type="submit" name="submit" value="Submit" class=" bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
											</div>
										</div>
										<div>
    										<label class="block text-sm font-medium text-gray-700"> Result </label>
											<div class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
												<div class="space-y-1 text-center">
													<?php
														if(isset($input_date)){
															echo getPreviousDates($input_date);
														}
													?>
												</div>
											</div>
										</div>
									</div>
									</div>
								</div>
							</form>
    					</div>
					</div>
				</div>
			</main>
		</div>
	</body>
</html>
