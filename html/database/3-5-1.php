<?php
	include 'mysql_connection.php';
	$data = array();
	$errors = array();
	$file_name = "";

	if(isset($_POST['submit']))
	{
		//validate input data
		foreach($_POST as $key => $value){
			if(empty($value)){
				$errors[$key] = $key." is required.";
			} else {
				switch($key){
					case 'firstName':
					case 'middleName':
					case 'lastName':
						if(preg_match('~[0-9]+~', $value)){
							$errors[$key] = $key." should all be characters";
						}
						break;
					case 'email':
						if(!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $value)){
							$errors[$key] = "invalid email address.";
						}
						break;
					case 'birthDate':
					case 'hireDate':
						// $date_arr  = explode('/', $value);
						// if (!checkdate((int)$date_arr[0], (int)$date_arr[1], (int)$date_arr[2])) {
						//     $errors[$key] = $key." should be a valid date.";
						// }
						break;
					case 'department':
					case 'boss':
						if(preg_match('~[a-zA-Z]+~', $value)){
							$errors[$key] = $key." value is not on selection.";
						}
						break;
					case 'password':
						if(strlen($value) < 5){
							$errors[$key] = $key." should be at least 5 characters.";
						}
						break;
					default:
						break;
				}
			}
		}

		//upload file
		if(isset($_FILES['photo'])){

			$file_name = $_FILES['photo']['name'];
			$file_temp = $_FILES['photo']['tmp_name'];
			$temp_extension = explode('.',$file_name);
			$file_extension = strtolower(end($temp_extension));
			$allowed_extensions = array("jpeg","jpg","png");

			if(in_array($file_extension,$allowed_extensions) === false){
				$errors['photo']="file not allowed, please choose a JPEG or PNG file.";
			} else {
				if(count($errors) <= 0){
					move_uploaded_file($file_temp,"photos/".$file_name);
				}
			}
		} else {
			$errors['photo'] = "Photo is required.";
		}

		if(count($errors) <= 0 ){
			/* insert to database */
			$insert_employee_sql = "insert into employees (first_name,last_name,middle_name,birth_date,password,email,photo,department_id,hire_date,boss_id)
			values ('".
				$_POST["firstName"]."','".
				$_POST["middleName"]."','".
				$_POST["lastName"]."','".
				$_POST["birthDate"]."','".
				md5($_POST["password"])."','".
				$_POST["email"]."','".
				$file_name."','".
				$_POST["department"]."','".
				$_POST["hireDate"]."','".
				$_POST["boss"].
			"')";
			// echo $insert_employee_sql;
			if($conn->query($insert_employee_sql)){
				$employee_id = $conn->insert_id;
				$insert_employee_position_sql = "insert into employee_positions (employee_id, position_id) values ('".$employee_id."','".$_POST["position"]."')";
				$conn->query($insert_employee_position_sql);

				header("Location:3-5-2.php?userData=$_POST");
			} else {
				// echo $insert_employee_sql;
			}
		}
	}
?>
<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://rsms.me/inter/inter.css">
		<link rel="stylesheet" href="../style.css">
	</head>
	<body>
		<div class="min-h-screen bg-white">
			<main>
				<!-- Side-by-side grid -->
				<div class="bg-white">
					<div class="max-w-md mx-auto py-12 px-4 sm:max-w-3xl sm:py-10 sm:px-6 lg:max-w-7xl lg:px-8">
						<a href="../" class="self-end text-base text-indigo-600 font-semibold  uppercase">Home</a>
						<br>
						<div class="mt-5 md:mt-0 md:col-span-2">
							<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
								<div class="shadow sm:rounded-md sm:overflow-hidden">
									<div class="px-4 py-3 bg-gray-50 text-left sm:px-6">
										<h3>Input user information. Then show it on the next page.</h3>
										<span class=" text-gray-500 text-sm">
											Create a form for basic user information. When you enter values in each input then press the submit button, it will show the inputted data in another page.
										</span>
									</div>
									<div class="px-4 py-5 bg-white space-y-6 sm:p-6">
									<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> First name </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="text" name="firstName" id="company-website" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300">
												</div>
											</div>
										</div>
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Middle name </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="text" name="middleName" id="company-website" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300">
												</div>
											</div>
										</div>
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Last name </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="text" name="lastName" id="company-website" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300">
												</div>
											</div>
										</div>
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Birthdate </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="date" name="birthDate" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300">
												</div>
											</div>
										</div>
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Email </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="email" name="email" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300">
												</div>
											</div>
										</div>
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Password </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="password" name="password" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300">
												</div>
											</div>
										</div>
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Photo </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="file" name="photo">
													<!-- <input type="file" name="photo" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300"> -->
												</div>
											</div>
										</div>
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Hire date </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="date" name="hireDate" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300">
												</div>
											</div>
										</div>
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Boss </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<select name="boss" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
														<?php
															if($bosses->num_rows > 0){
																while($row = $bosses->fetch_assoc()) {
																	echo "<option value='".$row["id"]."'".">".$row["first_name"]." ".$row["middle_name"]." ".$row["last_name"]."</option>";
																}
															} else {
																echo "<option value=''>No Boss Available</option>";
															}
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Department </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<select name="department" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
														<?php
															if($departments->num_rows > 0){
																while($row = $departments->fetch_assoc()) {
																	echo "<option value='".$row["id"]."'".">".$row["name"]."</option>";
																}
															} else {
																echo "<option value=''>No Departments Available</option>";
															}
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Position </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<select name="department" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
														<?php
															if($positions->num_rows > 0){
																while($row = $positions->fetch_assoc()) {
																	echo "<option value='".$row["id"]."'".">".$row["name"]."</option>";
																}
															} else {
																echo "<option value=''>No Positions Available</option>";
															}
														?>
													</select>
												</div>
											</div>
										</div>
										<?php
											if(count($errors) > 0){
												echo '<div class="font-medium text-red-600">Whoops! Something went wrong.</div>';
													echo '<ul class="mt-3 list-disc list-inside ">';
													foreach($errors as $error){
														echo "<li class='text-sm text-red-600'>$error</li>";
													}
													echo "</ul>";
												echo '</div>';
											}
										?>
										<div class="mt-1 flex items-left">
											<input type="submit" name="submit" value="Submit" class=" bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
										</div>
										</div>
									</div>
								</div>
							</form>
    					</div>
					</div>
				</div>
			</main>
		</div>
	</body>
</html>
