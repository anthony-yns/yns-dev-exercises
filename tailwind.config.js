const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors');

module.exports = {
  content:["./html/**/*.{html,js,php}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        amber: colors.amber,
        sky: colors.sky,
        teal:colors.teal,
        emerald:colors.emerald,
        lime:colors.lime,
        cyan:colors.cyan,
        teal:colors.teal,
        rose:colors.rose,
        violet:colors.violet,
      },
    },
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography'),require('@tailwindcss/aspect-ratio')],
}
