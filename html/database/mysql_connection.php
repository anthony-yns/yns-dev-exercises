<?php
    $servername = "yns-dev-exercises-mysql-server-1";
    $username = "root";
    $password = "secret";
    $database = "yns-dev-exercises";
    $departments = array();
    $positions = array();
    $bosses = array();
    // Create connection
    $conn = new mysqli($servername, $username, $password, $database);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);

    } else {
        /* check if database exists */
        $departments = getDepartments($conn);
        $positions = getPositions($conn);
        $bosses = getEmployees($conn);
    }

    function getDepartments($conn){
        $sql = "select * from departments";
        return $conn->query($sql);
    }

    function getPositions($conn){
        $sql = "select * from positions";
        return $conn->query($sql);
    }

    function getEmployees($conn){
        $sql = "select id,first_name, middle_name, last_name from employees";
        return $conn->query($sql);
    }
?>