<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://rsms.me/inter/inter.css">
		<link rel="stylesheet" href="../style.css">
	</head>
	<body>
		<div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
			<div class="lg:text-center">
				<p class="mt-20 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">Hello World!</p>
			</div>
			<a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="slef-center text-base text-indigo-600 font-semibold  uppercase">Back to Home</a>
		</div>

	</body>
</html>
