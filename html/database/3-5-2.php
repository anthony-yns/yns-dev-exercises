<?php
	include 'mysql_connection.php';
	session_start();
	// if(!isset($_SESSION['userData'])){
	// 	header("Location:3-5-4.php");
	// }
	$row_count = 1;
	$data = array();
	$current_page = isset($_GET['pageNumber']) ? (int)$_GET['pageNumber']:1;
	$previous_page = $current_page - 1;
	$next_page = $current_page + 1;
	$limit = 10;
	$from = ($current_page * $limit) - $limit;
	$to = ($current_page * $limit);
	//setup Pages
	$all_employee_query = "select * from employees";
	$result = $conn->query($all_employee_query);
	if($result->num_rows > 0){
		$pages = ceil(($result->num_rows/$limit));
	}

	//get datas
	$offset = ($current_page-1) * $limit;
	$employee_query = "select * from employees limit $offset, $limit";
	$result2 = $conn->query($employee_query);
	// $offset = $page_number * $limit;

?>
<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://rsms.me/inter/inter.css">
		<link rel="stylesheet" href="../style.css">
	</head>
	<body>
		<div class="min-h-screen bg-white">
			<main>
				<!-- Side-by-side grid -->
				<div class="bg-white">
					<div class="max-w-md mx-auto py-12 px-4 sm:max-w-3xl sm:py-10 sm:px-6 lg:max-w-7xl lg:px-8">
						<a href="../" class="self-end text-base text-indigo-600 font-semibold  uppercase">Home</a>
						<br>
						<div class="mt-5 md:mt-0 md:col-span-2">
							<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
								<div class="shadow sm:rounded-md sm:overflow-hidden">
									<div class="px-4 py-3 bg-gray-50 text-left sm:px-6">
										<h3>Show the user information using table tags. (PHP & SQL)</h3>
										<span class=" text-gray-500 text-sm">
											Create a new page for listing up all the user information (from the csv) using table tags.
										</span><br><br>
										<h3>Add pagination in the list page. (PHP & SQL)</h3>
										<span class=" text-gray-500 text-sm">
											Add pagination in the list page in order to show only 10 user information in each page.
										</span>
									</div>
									<div class="px-4 py-5 bg-white space-y-6 sm:p-6">
										<div>
    										<label class="block text-sm font-medium text-gray-700"></label>
											<div class="mt-1 flex justify-center px-6 pt-5 pb-6 rounded-md">
												<div class="space-y-1 text-center">
												<table v-if="displayedPosts.length > 0" class="min-w-full divide-y divide-gray-200">
														<thead class="bg-gray-50">
															<tr>
																<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
																	Name
																</th>
																<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
																	Birthdate
																</th>
																<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
																	Email
																</th>
																<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
																	Photo
																</th>
															</tr>
														</thead>
														<tbody class="border-1 rounded-lg bg-white divide-y divide-gray-200">
															<?php
																while($row = $result2->fetch_assoc()) {
																	echo "<tr>";
																		echo "<td>".$row["first_name"]." ".$row["middle_name"]." ".$row["last_name"]. "</td>";
																		echo "<td>".$row["birth_date"]. "</td>";
																		echo "<td>".$row["email"]. "</td>";
																		echo '<td class="px-6 py-4 whitespace-nowrap">';
																					echo '<div class="flex items-center">';
																						echo '<div class="flex-shrink-0 h-10 w-10">';
																							echo "<img class='h-10 w-10 rounded-full' src='photos/".$row["photo"]."' alt=''>";
																						echo '</div>';
																					echo '</div>';
																				echo '</td>';
																	echo "</tr>";
																}
															?>
														</tbody>
													</table>
													<div class="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
														<div class="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
															<div>
															
															</div>
															<div>
																<nav class="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">
																	<?php
																		if($current_page > 1){
																			echo '<a href="?pageNumber='.($current_page-1).'" class="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">'.
																				'<span class="sr-only">Previous</span>'.
																				'<svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">'.
																					'<path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />'.
																				'</svg>'.
																			'</a>';
																		}

																		for($i=1; $i <= $pages; $i++){
																			echo '<a href="?pageNumber='.$i.'" class="bg-white border-gray-300 text-gray-500 hover:bg-gray-50 hidden md:inline-flex relative items-center px-4 py-2 border text-sm font-medium">'.$i.'</a>';
																		}

																		if($current_page < $pages){
																			echo '<a href="?pageNumber='.($current_page+1).'" class="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">'.
																				'<span class="sr-only">Previous</span>'.
																				'<svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">'.
																					'<path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />'.
																				'</svg>'.
																			'</a>';
																		}
																	?>
																</nav>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									</div>
								</div>
							</form>
							<a href="logout.php" class="self-end text-base text-indigo-600 font-semibold  uppercase">Logout</a>
    					</div>
					</div>
				</div>
			</main>
		</div>
	</body>
</html>
