<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://rsms.me/inter/inter.css">
		<link rel="stylesheet" href="../style.css">
		<?php
            $servername = "yns-dev-exercises-mysql-server-1";
            $username = "root";
            $password = "secret";
            $database = "yns-dev-exercises";
            $question_count = 0;
            $score = 0;
            $questions = array();
            $choices = array();
            // Create connection
            $conn = new mysqli($servername, $username, $password, $database);

             // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } else {
                $questions = getQuestions($conn);
            }

            function getQuestions($conn){
                $sql = "select * from questions order by rand(now())";
                return $conn->query($sql);
            }

            function getChoices($conn){
                $sql = "select * from question_choices";
                return $conn->query($sql);
            }

            function getAnswers($conn){
                $sql = "select * from question_answers";
                return $conn->query($sql);
            }

            //If submit button is clicked
            if(isset($_POST['submit']))
			{
                $answers = getAnswers($conn);
                foreach($_POST as $key => $value){
                    while($row = $answers->fetch_assoc()) {
                        if($key == $row["question_id"]){
                            if($value == $row["question_choice_id"]){
                                $score++;
                            }
                            break;
                        }
                    }
                }
            }
        ?>
	</head>
	<body>
		<div class="min-h-screen bg-white">
			<main>
				<!-- Side-by-side grid -->
				<div class="bg-white">
					<div class="max-w-md mx-auto py-12 px-4 sm:max-w-3xl sm:py-10 sm:px-6 lg:max-w-7xl lg:px-8">
						<a href="../" class="self-end text-base text-indigo-600 font-semibold  uppercase">Home</a>
						<br>
						<div class="mt-5 md:mt-0 md:col-span-2">
							<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
								<div class="shadow sm:rounded-md sm:overflow-hidden">
									<div class="px-4 py-3 bg-gray-50 text-left sm:px-6">
										<h3>Quiz with three multiple choices</h3>
										<span class=" text-gray-500 text-sm">
                                            Provide 10 questions. Show the score after.<br>
                                            Questions should be stored in the database and should be randomized.<br>
                                            Create wireframe and schema before developing, put them in [Your Name] (Quiz) sheet

										</span>
									</div>
									<div class="px-4 py-5 bg-white space-y-6 sm:p-6">
										<div>
                                            <h3>Test Quiz</h3>
    										<label class="block text-sm font-medium text-gray-500"> Analyze and answer the questions carefully. Whenever you're ready please start.</label>
                                            <?php if($score > 0){ echo "<p border=1>You're score is: ".$score."/10</p>"; }?>
											<div class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
												<div class="space-y-1 text-left">
                                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                                                        <?php
                                                            if ($questions->num_rows > 0) {
                                                                // output data of each row
                                                                while($row = $questions->fetch_assoc()) {
                                                                    $question_count++;
                                                                    echo "$question_count".". ". $row["question"]."<br>";
                                                                    //display choices
                                                                    $choices = getChoices($conn);
                                                                    if ($choices->num_rows > 0) {
                                                                        while($row2 = $choices->fetch_assoc()) {
                                                                            if($row2["question_id"] == $row["id"]){
                                                                                echo '&nbsp&nbsp<input type="radio" name="'.$row["id"].'" value="'.$row2['id'].'">'.$row2['choices'].'<br><br>';
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                echo "0 results";
                                                            }
                                                        ?>
                                                        <br>
                                                        <input type="submit" class=" bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" name="submit" value="Submit"><br>
                                                    </form>
												</div>
											</div>
										</div>
									</div>
									</div>
								</div>
							</form>
    					</div>
					</div>
				</div>
			</main>
		</div>
	</body>
</html>
