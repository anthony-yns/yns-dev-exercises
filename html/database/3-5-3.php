<?php
	include 'mysql_connection.php';
	$errors = array();
	$authenticated = false;
	$authenticated_user = array();
	if(isset($_POST['submit']))
	{
		//validate input data
		foreach($_POST as $key => $value){
			if(empty($value)){
				$errors[$key] = $key." is required.";
			}
		}

		if(count($errors) <= 0){
			/* Check if employee exists */
			$hashed_password = md5($_POST['password']);
			$authenticate_query = "select * from employees where first_name ='".$_POST["id"]."' and password ='".$hashed_password."'";
			$result = $conn->query($authenticate_query);

			if($result->num_rows > 0){
				session_start();
				$row = $result->fetch_assoc();
				$_SESSION['userData'] = $row;
				header("Location:3-5-2.php");
			} else {
				$errors['attempt'] = "Invalid ID or Password";
			}
		}
	}
?>

<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://rsms.me/inter/inter.css">
		<link rel="stylesheet" href="../style.css">
	</head>
	<body>
		<div class="min-h-screen bg-white">
			<main>
				<!-- Side-by-side grid -->
				<div class="bg-white">
					<div class="max-w-md mx-auto py-12 px-4 sm:max-w-3xl sm:py-10 sm:px-6 lg:max-w-7xl lg:px-8">
						<a href="../" class="self-end text-base text-indigo-600 font-semibold  uppercase">Home</a>
						<br>
						<div class="mt-5 md:mt-0 md:col-span-2">
							<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
								<div class="shadow sm:rounded-md sm:overflow-hidden">
									<div class="px-4 py-5 bg-white space-y-6 sm:p-6">
										<div class="grid grid-cols-6 gap-6">
											<div class="col-span-2 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Username </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="text" name="id" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300" placeholder="e.g Juan">
												</div>
											</div>
											<div class="col-span-3 sm:col-span-2">
												<label for="company-website" class="block text-sm font-medium text-gray-700"> Password </label>
												<div class="mt-1 flex rounded-md shadow-sm">
													<input type="password" name="password" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-1/2  rounded-lg sm:text-sm border-gray-300">
												</div>
											</div>
										</div>
										<?php
											if(count($errors) > 0){
												foreach($errors as $error){
													echo '<div class="font-medium text-red-600">'.$error.'</div>';
												}
											}
										?>
										<div>
											<div class="mt-1 flex items-left">
												<input type="submit" name="submit" value="Login" class=" bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
											</div>
										</div>
									</div>
									<div class="px-4 py-3 bg-gray-50 text-left sm:px-6">
										<h3>Create a login form and embed it into the system that you developed (PHP & SQL)</h3>
										<span class=" text-gray-500 text-sm">
										Login form must have 2 text boxes for user ID and password.
										<br>If there is any error in validation, the error message(s) will be shown.
										<br>After login, user information will be saved in the session and keep login until the user logs out or closes the web browser.
										</span>
									</div>
								</div>
							</form>
    					</div>
					</div>
				</div>
			</main>
		</div>
	</body>
</html>
