<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://rsms.me/inter/inter.css">
		<link rel="stylesheet" href="style.css">
		<?php
			$excercises= array(

				'HTML & PHP' => array(
					'<a class="text-lg font-medium text-warm-gray-900" href="html_php/1-1.php">1-1 Show Hello World</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="html_php/1-2.php">1-2 The four basic operation</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="html_php/1-3.php">1-3 Show greatest common divisor</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="html_php/1-4.php">1-4 Solve the fizzbuzz</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="html_php/1-5.php">1-5 Show 3 days from the input date.</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="html_php/1-6.php">1-6 to 1-9</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="html_php/1-13.php">1-13 Login Page</a>',
				),

				'Javascript' => array(
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-1.html">2-1 Show alert</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-2.html">2-2 Confirm dialog and redirection</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-3.html">2-3 The four basic operation</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-4.html">2-4 Show prime numbers</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-5.html">2-5 Show in the label</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-6.html">2-6 Press the button</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-7.html">2-7 Show alert when click image</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-8.html">2-8 Show alert when click link</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-9.html">2-9 Change text background</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-10.html">2-10 Scroll screen when press buttons</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-11.html">2-11 Change background using animation</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-12.html">2-12 Show another image mouse hover</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-13.html">2-13 Change the size of images</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-14.html">2-14 Show images according to the option</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="javascript/2-15.html">2-15 Show current date and time (real time)</a>'
				),

				'Database' => array(
					'<a class="text-lg font-medium text-warm-gray-900" href="database/3-2.sql.txt">3-2 create table</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="database/3-3.sql.txt">3-3 insert, update, delete query</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="database/3-4.sql.txt">3-4 Sql problems</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="database/3-5-1.php">3-5-1 Add Information</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="database/3-5-1-display.php">3-5-1 Display Information</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="database/3-5-3.php">3-5-3 Authentication</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="database/3-5-2.php">Show All Users (Need to login/add information first)</a>'
				),

				'Advanced_sql' => array(
					'<a class="text-lg font-medium text-warm-gray-900" href="advanced_sql/4-1.sql.txt">4-1 Selecting by age using birth date</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="advanced_sql/4-2.sql.txt">4-2 Create data for the given scenario</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="advanced_sql/4-3.sql.txt">4-3 Using Case conditional statement</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="advanced_sql/4-4.sql.txt">4-4 Create sql that will display the OUTPUT</a>'
				),

				'Practice systems/programs' => array(
					'<a class="text-lg font-medium text-warm-gray-900" href="practice_systems_programs/6-1.php">6-1 Quiz with three multiple choices</a>',
        			'<a class="text-lg font-medium text-warm-gray-900" href="practice_systems_programs/6-1.sql.txt">6-1 Quiz with three multiple choices .sql file</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="practice_systems_programs/Anthony Famorcan Quiz sheet.pdf">6-1 Pdf copy of Wireframe and ERD</a>',
					'<a class="text-lg font-medium text-warm-gray-900" href="practice_systems_programs/6-2.html">6-2 Calendar</a>'
				),
			);
		?>
	</head>
	<body>
		<div class="min-h-screen bg-white">
			<main>
				<!-- Side-by-side grid -->
				<div class="bg-white">
					<div class="max-w-md mx-auto py-12 px-4 sm:max-w-3xl sm:py-10 sm:px-6 lg:max-w-7xl lg:px-8">
						<div class="divide-y divide-warm-gray-200">
							<?php foreach($excercises as $key => $value){ ?>
						<section class="mt-16 pt-16 lg:grid lg:grid-cols-3 lg:gap-8" aria-labelledby="contact-heading">
								<div>
									<h2 id="contact-heading" class="text-2xl font-extrabold text-warm-gray-900 sm:text-3xl">
									<?php echo $key; ?>
									</h2>
									<!-- <p class="mt-4 text-lg text-warm-gray-500">Can’t find the answer you’re looking for? Reach out to our <a href="#" class="font-medium text-cyan-700 hover:text-cyan-600">customer support</a> team.</p> -->
								</div>
								<div class="mt-8 grid grid-cols-1 gap-12 sm:grid-cols-2 sm:gap-x-8 sm:gap-y-12 lg:mt-0 lg:col-span-2">
									<div>
										<h3 class="text-lg font-medium text-warm-gray-900">
										Excercises
										</h3>
										<dl class="mt-2 text-base text-warm-gray-500">
											<?php
												foreach($value as $excercise){
													echo
														'<div>
															<dd>'.
																$excercise.'
															</dd>
														</div>';
												}
											?>
										</dl>
									</div>
								</div>
							</section>
							<?php }?>
						</div>
					</div>
				</div>
			</main>
		</div>
	</body>
</html>